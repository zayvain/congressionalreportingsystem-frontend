import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  
  constructor(
    private router: Router,
    private authService: AuthService, 
    private tokenStorage: TokenStorageService,
    private toastr:ToastrService
    ) { }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
    }
    
  }
  
  onSubmit() {
    this.authService.login(this.form).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        this.router.navigate( ["/mainpage"] );
      },
      err => {
        this.errorMessage = err.error.message;
        if(this.errorMessage == null){
          this.errorMessage = "Credentials incorrect."
        }
        this.isLoginFailed = true;

      }
      
    );
  
  }

  reloadPage() {
    window.location.reload();
  }
  back(){
    this.router.navigate( ["/home"] );
  }
  Dashboard(){
    this.router.navigate(["/mainpage"])
  }
  signup(){
    this.router.navigate(["/signup"])
  }
  forgotPass(){
    this.router.navigate(["/forgotpassword/email"])
  }
  
}
