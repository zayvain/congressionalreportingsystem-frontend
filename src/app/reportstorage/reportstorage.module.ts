import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportstorageRoutingModule } from './reportstorage-routing.module';
import { ReportstorageComponent } from './reportstorage.component';
import { FormsModule } from '@angular/forms';
import { NgbPaginationModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { UploadComponent } from './upload/upload.component';


@NgModule({
  declarations: [ReportstorageComponent, UploadComponent],
  imports: [
    CommonModule,
    ReportstorageRoutingModule,
    FormsModule,
    NgbPaginationModule,
    NgbModalModule,
    RouterModule,
  ],
  entryComponents:[UploadComponent]
})
export class ReportstorageModule { }
