import { Component, OnInit } from '@angular/core';
import { PdfReport } from '../_models/pdfreport';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PdfreportService } from '../_services/pdfreport.service';
import { UploadComponent } from './upload/upload.component';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-reportstorage',
  templateUrl: './reportstorage.component.html',
  styleUrls: ['./reportstorage.component.css']
})
export class ReportstorageComponent implements OnInit {
  pdfReports:any;
fileUrl;
  constructor(
    private modalService: NgbModal,
    private pdfReportService:PdfreportService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.LoadPDFs();
  }
  openUploadFrom(){
    this.modalService.open(UploadComponent,{ size: 'lg',centered: true,});
  }
  LoadPDFs(){
    this.pdfReportService.getAllPdfReports().subscribe(data => {
      this.pdfReports = data[1];

      const blob = new Blob([this.pdfReports.pic], { type: 'application/pdf' });
  
      this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
      console.log(this.pdfReports);
      console.log(this.fileUrl);
    });
  }
}
