import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models/user';
import { UserService } from 'src/app/_services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { DeleteuserComponent } from './deleteuser/deleteuser.component';

@Component({
  selector: 'app-activeusers',
  templateUrl: './activeusers.component.html',
  styleUrls: ['./activeusers.component.css']
})
export class ActiveusersComponent implements OnInit {
  usersData: User[];
  searchText: string;

    //initializing p to one
    p: number = 1;
    
  constructor(
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private toastr:ToastrService
  ) {}

  ngOnInit() {
    this.userService.getAllUsers().subscribe(
      data => {
        this.usersData = data;
      
      },
     
    );
  }
  // openUserForm(user: User) {
  //   const modalRef = this.modalService.open(EdituserComponent,{ size: 'lg',centered: true,});
  //   modalRef.componentInstance.user = user;
  //   modalRef.result.then(result => {
  //     if (!result) return;
  //     if (user) {
       
  //       this.userService.updateUser(result).subscribe(
  //         (response: User) => {
  //           if (response) {
  //             this.toastr.success("Update Success.")
  //           }
  //         },
  //         () => this.toastr.error("Update Failed.")
  //       );
  //     } else {
       
  //       this.userService.addUser(result).subscribe(
  //         (response: User) => {
  //           if (response) {
  //             this.toastr.success("User Added Successfully.")
  //           }
  //         },
  //         () => this.toastr.error("User Creation Failed.")
  //       );
  //     }
  //   });
  // }
  onDelete(user: User) {
    const modalRef = this.modalService.open(DeleteuserComponent);
    modalRef.componentInstance.user = user;
    modalRef.result.then(result => {
      if(!result) return null;
      this.userService.deleteUser(result).subscribe(
        response => {
          if (response) {
            this.toastr.success("Delete Success");
          }
        },
        () => this.toastr.error("Delete Failed")
      );
    });
  }

}
