import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewdataRoutingModule } from './viewdata-routing.module';
import { ViewdataComponent } from './viewdata.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [ViewdataComponent],
  imports: [
    CommonModule,
    ViewdataRoutingModule,
    FormsModule,
    RouterModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
  ],
  entryComponents:[]
})
export class ViewdataModule { }
