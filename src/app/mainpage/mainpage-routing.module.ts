import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageComponent } from './mainpage.component';


const routes: Routes = [
  {
    path: '', component: MainpageComponent, children: [
      { path: 'dashboard', loadChildren: () => import(`../dashboard/dashboard.module`).then(m => m.DashboardModule) },
      { path: 'reports', loadChildren: () => import(`../reports/reports.module`).then(m => m.ReportsModule) },
      { path: 'insertdata', loadChildren: () => import(`../insertdata/insertdata.module`).then(m => m.InsertdataModule) },
      { path: 'programs', loadChildren: () => import(`../programs/programs.module`).then(m => m.ProgramsModule) },
      { path: 'manageusers', loadChildren: () => import(`../manageusers/manageusers.module`).then(m => m.ManageusersModule) },
      { path: 'reportstorage', loadChildren: () => import(`../reportstorage/reportstorage.module`).then(m => m.ReportstorageModule) },
      { path: 'profile', loadChildren: () => import(`../profile/profile.module`).then(m => m.ProfileModule) },
      {  path: '', redirectTo: 'dashboard', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainpageRoutingModule { }
